/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
  ],
  theme: {
    screen: {
      tablet: "640px",
      laptop: "992px",
      desktop:"1280px"
    },
    extend: {},
  },
  plugins: [],
}
